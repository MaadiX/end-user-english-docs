# Domain configuration in MaadiX

## First step: get your domain

Every website or application needs a good domain name. If you already have your own domain, MaadiX allows you to activate it easily and to configure it for your applications.

If you don't have your own domain yet, here are some sample providers where you can easily register it (but there are many more [1]!):

* [gandi.net](https://www.gandi.net/)
* [namecheap.com](https://www.namecheap.com/domains/registration.aspx)
* [Njal.la](https://njal.la/)

We know that choosing a good name isn't always easy. That's why, while you're deciding what your domain name is going to be, you can still use MaadiX and most of its applications by using https://MyMaadixServerName.maadix.org, where 'MyMaadixServerName' matches the name you chose when you purchased your MaadiX server.
Some applications, however, such as RocketChat, Only Office Online, Collaborate Online, Discourse, Mailtrain as well as the email server and mailing lists can be used only if you have your own domain name.

If you already have your own domain, follow the instructions below to activate it.

## Activate your domain

From your control panel, go to the tab '**Domains**' -> '**Add a new domain**'.

A form will be displayed containing the following fields:

* **Domain Name**: Enter the full name of your domain or sub-domain (for example: example.com or docs.example.com).

For each domain or subdomain you activate, a folder will be created on your server with the location `/var/www/html/example.com/`. You must upload your website or application to this folder so that it is accessible from the browser by visiting *example.com*.

* **Enable mail server for this domain**: If you want to use the internal mail server for the domain you are creating, this option has to be activated. Otherwise, if you want the email to be managed by another external server, leave it off. You can change this option at any time from the domain editing page.

* **Webmaster**: you can assign a webmaster (web administrator) in each domain or subdomain that you activate in MaadiX. By clicking on the drop down 'Assign Webmaster', all regular user accounts with enabled SFTP access will be listed.

The webmaster will have permissions to create, delete or modify files within the `/var/www/html/example.com/` folder, where he/she can create the web application. This user will have access via SFTP to this folder and its home, but will not be able to access or see the rest of the files or folders on your server.

We at MaadiX recommend you to create a webmaster user (ordinary user + SFTP), especially if you want to share access with someone to work on the web, application or contents of the `/var/www/html/example.com/` folder, so that you never have to share access as a Superuser, whose privileges are unlimited over the system.

![Add domain](img/en/domains/add-domain.png)


## Configure your domain's DNS so that it points to your server

Your server includes a system that automatically checks if your domain is pointing correctly to your server. If it is, the same system will proceed with the creation of all the necessary configurations. If this is not the case, it will periodically repeat the same check until an affirmative response is received. To find out if the activation and configuration process of the domain has been successfully completed, look at the icon **Web server** on the page **View Domains**. If the icon is green (Enabled) the configuration to be able to create a web has been carried out successfully.

![Screenshot](img/en/domains/web-server-enabled.png)

You must modify your DNS in order to point your domain to your server. DNS (Domain Name System) servers are those that transform domain names, designed for human understanding, into numbers that correspond to the IP addresses of the different machines connected and publicly accessible on the Internet.

By clicking on 'View DNS' in your domain column, you will find the settings required to make your domain work for both your web application (A-Registration) and your mail server (MX- and SPF-Registration).

![View DNS](img/en/domains/view-dns-link.png)

You must enter this data in the corresponding section of the DNS configuration within the **client area of your domain provider** (this phase must be completed outside your MaadiX server). There will probably be a link or tab, perhaps in the menu, that says something like *DNS*, *Edit DNS records* or *Edit DNS zone*. You can check the [DNS](dns) section for detailed instructions on the different types of records required for all services to work properly.


 ![Required DNS](img/en/domains/required-dns.png)


Once the changes are made, go back to the DNS settings page in your MaadiX control panel by clicking "View DNS" in the domain column.
Remember that the process of propagating new DNS can take up to 48 hours, so it is normal that for some time the configuration continues to be incorrect even if you have changed it.

## HTTPS

All domains that you activate through the control panel will always have an SSL certificate activated and will be accessible through the address: https://yourdomain.com

The creation and configuration of certificates is automated and it is completed using [Let's Encrypt](https://letsencrypt.org), along with the entire process of activating and configuring domains in your system.

No additional configuration is needed to activate HTTPS for your domain. The certificates are valid for three months and are automatically renewed.


## Upload your website or application to your own domain

Once the green icon "Enabled" appears for your domain's web server, you can upload your web application files to the newly created `/var/www/html/example.com/` folder. You can do this very easily with an SFTP client (for example, [Filezilla](https://filezilla-project.org/)). Once they are there, you can visit them from the browser in your *example.com* domain.

You can find more instructions here: [create your website or application](create-web).


## Start using your email server

If you have checked the box 'Enable mail server for this domain', you can also start using your email server. Go to the *Mail Accounts* section to open new accounts by clicking on the '*Add a new account*' button at the top right of this page.
Remember that the MX and SPF records have to be correctly configured to point to your server.

You can find more guidelines here: [create and manage email accounts](email).


----

[1]: Complete list of domain name registrars accredited by ICANN. [See list](https://www.icann.org/registrar-reports/accredited-list.html).
