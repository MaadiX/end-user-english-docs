# Server Set Up

After the process of creating a virtual machine on MaadiX is complete, the new server will send an email notification to the email account that has been configured for the administrator.

The email contains instructions on how to access the server, including passwords. These passwords are generated locally by the server, so no one but the server knows them.

However (at least for the time being), the process of sending passwords by mail is done in plain text, which makes it absolutely essential to change them immediately for security reasons. That is why MaadiX will force passwords reset through an activation process at the time of the first authentication in the control panel.

# Activation

The first time the control panel is accessed, only the activation page will be displayed. On this page you will have to change the password of two different system accounts:

* The administration account for the control panel
* The system root account (Superuser)

Until this process is completed, the other sections of the control panel will not be accessible and the root account will not be able to access the system via SSH or SFTP.

This is an important security measure, which must also be applied to any other applications that have been installed and require a username and password.

It is extremely important that the e-mail associated with the control panel administration account is valid and that you have access to it, since the server will send to this email account all the notifications, indications and instructions to recover the control panel access key in case you forget it.

# Details

The control panel start window displays internal statistics about system resource usage. It also shows information about the DNS settings for your server domain name (Server name or FQDN).

![Screenshot](img/en/panel-de-control.png)
