# Etherpad

Etherpad Lite is an application external to the control panel that enables real-time, multi-user collaborative editing of documents (Pads).

In addition, the servers created with MaadiX include by default, along with this application, an extension that enables private work areas. Thus, you can choose for the created documents to be accessible to the general public or only to users who have an activated account.

# Etherpad admin

The Etherpad Lite application includes its own administration panel that allows to:

* Edit preferences.
* Install or uninstall plug-ins.
* Re-start the application.

The administration area of this application is available at the address:
https://*myMaadixServername*.maadix.org/etherpad/admin/

If you have your own domain activated on the server:
https://*mydomain*.com/etherpad/admin/

To access this area, you will have to insert the administrator's name and the password of the application (this information is included in the email you receive automatically once the tool is activated).

![Login in the administration area](img/etherpad/login_admin.png)

Only the admin user can access this area, where he or she has access to all groups and users created in the system. The password is not valid for operating from the front-end.

## Change the admin's password

Once inside the administrator area, it is recommended that you change the password. To do so, click on the 'Settings' tab in the left menu.
When you do so, a plain text file containing all the parameters of your installation will open.
Scroll to the end of this file, where you will find lines similar to the following:

      "users": {
        "admin": {
        "password": "yourpasswordhere",
        "is_admin": true
        }
      }

1. Change the value of the password to your new password, being careful to put it between quotation marks.
2. Click 'Save Settings'.
3. Click 'Restart Etherpad'.

![Change etherpad password](img/ch-paswd.png)


# Etherpad Private Area

MaadiX has developed an Etherpad Lite plugin, [ep_maadix](https://github.com/MaadixNet/ep_maadix), that enables you to create private workspaces, and which is installed by default along with the application.
From the Etherpad Lite administration area you can set configuration preferences for the installation.

Clicking on the 'Users and groups' tab in the left column, the following options will appear:

* **Allow users to recover lost password**: Enabling this option will allow users to retrieve their password. In general, it is advisable to leave it on, as this allows users to reset their password themselves, avoiding having to send it by mail or another channel.

* **Allow users to register**: If this option is activated, anyone can create an account without receiving an invitation. Otherwise, only users with a valid invitation will be able to access the application. If a user registers, he or she will not be able to access the groups already created until receiving an invitation, but to create new groups, and new Pads and to invite users to his or her group.

* **Allow public pads**: allows the creation of public Pads without the need to have a user activated or to belong to any group. Private groups and pads are still available even if this option is enabled. If you want to avoid that anyone can create new Pads, you can deactivate this option. You have to restart the service to apply this new setting, go to 'Settings' and click 'Restart Etherpad'.

![Users and groups settings](img/etherpad/settings_plugin.png)


## Create users

From the administrator area you can create groups and invite users. If you have chosen not to allow users to register without an invitation, you will have to create at least one account. The user thus created will be able to start managing groups from the front-end of the application. Remember that the administrator user's credentials are not valid to operate from the front-end.

To create groups and invite users from the administrator area, click on the 'Manage Groups' or 'Manage User' tabs, which you can find in the header of the 'Users and groups' section on the left column (main page of the plugin).

![Create users](img/etherpad/users_groups.png)

## Etherpad front-end

Users with an activated account can manage groups, invite other users and create and edit documents (Pads) from the front-end.

The administration area of the application is available at the address:
https://*myMaadixServername*.maadix.org/etherpad/

or if you have your own domain name activated on the server:
https://*mydomain*.com/etherpad/

To access the private area, you must identify yourself by clicking on 'Login' at the top right of the page.

![Access front-end](img/etherpad/login_front-end.png)

### Groups

Private Pads and users must be associated with a group.
The same user can belong to one or several groups and can create its own groups.
If a user does not belong to any group, he or she will have to create one before being able to create documents or invite new users

You can check the groups you have access to or create new ones by clicking on 'My groups', once inside the application (Login).

#### How to create a private group

Under "Create a new private group" insert a group name (this name must not yet exist in the system) and click the "Create" button. The new group will appear on the same page. Once this is done, you can start inviting other users or creating new documents.

### Invite Users

To add users to a group, click "View / Add Users" from the table on the "My groups" page. In the field "Invite user to this group" insert a valid email address of the user you want to create. If the inserted address is not yet registered in the system, a confirmation email will be sent to the new user with instructions on how to activate the account. You must also choose the role you want to assign to the new user for this group.

### Roles

The role assigned to a user only applies to a certain group. A user can have access to several groups with different roles in each of them. If a user creates a new group, his role for that group will always be 'Admin'.

A user can never assign a role higher than his own role within a group.

The roles available are:

   **Group Author**: can create and edit Pads

   **Group Manager**: can create / edit / delete Pads and invite / remove Users

   **Group Admin**: can create / edit / delete Pads, invite / delete Users and delete the whole group

The new guest user will appear in the table below. The role assigned to the user can be changed and edited later.

### How to create a private Pad

Pads must be created from a specific group's Pad list page, as each Pad can only belong to a certain group. To create a new Pad, click "View / Add Pads" in the table on the "My groups" page. On the new page that opens, insert the name of the Pad you want to create in the "Add a Private Pad to this Group" field and click the "Create" button. The new Pad will appear in the table below.

The name of the Pad must be unique to each group.

### Visiting existing Pads

To open a Pad that has already been created, click on "View / Add Pads" in the table on the "My groups" page, on the line corresponding to the group in which the document was created. You will find a list of all the Pads sorted by the last edit date of the selected group.
You can reverse this order so that the oldest editions are shown first, or reorder the list in alphabetical order using the arrows in the corresponding columns.

For further information follow [this link](https://github.com/ether/etherpad-lite/wiki) to find the official documentation of the Etherpad Lite application in full.
