# Introduction

Mailman is a software that allows you to create and manage mailing lists and newsletters, which consists of three different web interfaces.

* **Administration**
* **List management**
* **List files**

When the installation of the Mailman application on your server is complete, you will be sent an email that includes the username and password of the application's administrator.

# Changing the password

As for all other applications you install on the server, the first thing you will do is change the password of the administrator user for security reasons. To do this, click on the user icon at the top right and select 'Account'. This will open a page that will allow you to change the password, as well as configure other settings.

![Change password](img/mailman/edit-account.png)

# Setting up domain

Mailing lists operate under their own domain or sub-domain. This means that you cannot create mailing lists using the same domain you are using for email accounts. In case you are using the domain example.com for email accounts (admin@example.com, info@example.com....), you cannot use it to create mailing lists. What you will have to do is create a subdomain of example.com and configure the DNS before starting to create lists.

# Activating domain or subdomain in Mailman

To register a domain or subdomain and start creating mailing lists or newsletters, access the Mailman interface that you will find at https://myMaadixServername.maadix.org/mailman, where 'myMaadixServername' will be the name of your subdomain in MaadiX.
Once authenticated ('Login' tab at the top right of the window), the 'Domains' tab will appear in the top menu. Access this page and click on the 'Add Domain' button.
As an example we will use lists.example.com, but you can assign the name you prefer (news.example.com, info.example.com...).

![Add_domain](img/mailman/add-domain.png)


You will have to fill in the following three fields:

* **Mail Host**: the name of the domain or subdomain under which lists can be created (in our case lists.example.com, for which we will then have to create the corresponding DNS entries)
* **Description**: a description for the domain or sub-domain (optional)
* **Web Host**: the web address where it will be possible to access the options for the lists of this subdomain. You can leave the default value of mynamemaadix.maadix.org or add another domain or subdomain, provided that it is activated in the control panel and it works correctly. This will be the web address that will be sent to subscribers for actions such as confirming subscription, viewing public lists, or others.

In the case of this example, the **Web Host** could be the domain example.com or even the same subdomain _lists.example.com_, which must have the DNS correctly configured to point to your server, and it will have to be activated in the control panel. If you decide to use the same domain or subdomain as lists as the address for the graphical interface, **it is very important that you do not activate the mail server for it**. You must then activate the subdomain _lists.example.com_ in the control panel, within the section **'Add Domains', without checking the box 'Activate mail server for this domain'**.
This way, the necessary configuration will be created so that the subdomain _lists.example.com_ can be visited from the browser, without activating the mail server. If you were to activate the mail server for a domain that you wanted to use for lists, you might experience problems in delivering the messages.


# Creating mailing lists or newsletters

Once a domain is added to the Mailman application, you can create one or more mailing lists or newsletters under the same domain.
To do this, visit the 'Lists' tab in the menu above and then click on 'Add new List'

![Add_list](img/mailman/add-list.png)

You will have to fill in the following fields:

* **List Name**: the name of the list. In the example, we will create the 'info' list.
* **Mail Host**: the domain for the lists. In the drop-down menu we choose the newly created subdomain lists.example.com.
* **Initial list owner address**: we assign an email account as owner / administrator of the list.
* **Advertise this list?**: this option sets whether the list will be publicly visible in the list of created lists or not. To make it visible, we will choose 'Advertise this list in list index', while if we choose 'Hide this list in list index' only the administrator will see it.
* **List Style**: the type of list you want to create. The three options you can choose here are: 'Announce only mailing list style'; 'Ordinary discussion mailing list style'; or 'Discussion mailing list style with private archives'
* **Description**: an informative description of the list (optional).

Once this phase is completed, we will have created the list. Next, we'll need to set our configuration preferences.

**Setting up list file parameters**

You can choose whether messages sent to subscribers are kept and stored and, if so, whether they will be visible to anyone through a public link or only accessible to authorized persons.

En Settings > Archiving > Archive policy:

* **Public archives**: Anyone can consult them through the web interface of your application installation.
* **Private archives**: the history is saved, but only the administrators can access it.
* **Do not archive this list**: the history of sent messages is not saved.

![Archive_policy](img/mailman/mailman-archive.png)

**Notification of subscription changes**

Under Settings > Automatic Responses > Notify admin of membership changes, check the box if you want the administrator to be notified of membership additions and deletions.

![List_changes](img/mailman/list-changes.png)


Once the list is created, you must follow a specific configuration process to turn it into a newsletter or a mailing list.

# Setting up a newsletter

Remember that the newsletter is characterized by its unidirectionality: only expressly authorized accounts can send messages (as opposed to mailing lists, in which everyone can participate).

**Setting up subscription parameters**

In Settings > Subscription Policy, choose one of the following options from the drop-down menu:

* **Open**: anyone can be added to the newsletter without confirmation.
* **Confirm**: subscriptions must be confirmed from a valid email when applying for registration.
* **Moderate**: a moderator has to manually authorize each registration.
* **Confirm then Moderate**: send first a confirmation email to the user and a moderator needs to authorize the registration.

We recommend the 'Confirm' option so that no email addresses can be added without consent.

![Sub_policy](img/mailman/sub-policy.png)

**Setting up parameters for incoming messages**

Determines how messages addressed to the list by member and non-member users are treated. Since this is a newsletter, any incoming message whose sender is not authorized will be denied delivery.

In Settings > Message Acceptance, define the following parameters:

* **Default action to take when a member posts to the list: Discard** (all messages from subscribed users will be discarded).
* **Default action to take when a non-member posts to the list: Discard** (all messages from unsubscribed users will be discarded).

With this setting, any message addressed to the list by an unauthorized sender will be automatically rejected without notifying the person. If you prefer to be notified, replace the Discard option with Reject (with notification).

![Inbox_messages](img/mailman/inbox-messages.png)

**Setting up newsletter mailings**
At this time, no person is authorized to send mail, so it is necessary to grant this permission to at least one mail account, in order to make the mailings.
Under 'Users' > 'Members' you will find the list of subscribers.

Choose the email account you want to use to send the newsletters, and click on 'Member Options'. If the account is not present in the list, you will have to add it first (Mass operations > Mass subscribe).

At the bottom of the page select 'Accept immediately' as the moderation parameter, so that the selected account can send the newsletters. Remember that you can authorize more than one account for the same list.

![Newslettering](img/mailman/send-newsletter.png)

# Setting up a mailing list

Once the list is created, you must follow a specific configuration process to convert it into a mailing list. Remember that the mailing list is characterized by offering all users the possibility to participate (it is not unidirectional like the newsletter).

**Setting up subscription parameters**

In Settings > Subscription Policy, choose one of the following options from the drop-down menu:

* **Open**: Anyone can be added to the list without confirmation.
* **Confirm**: subscriptions must be confirmed from a valid email when applying for registration.
* **Moderate**: a moderator has to manually authorize each registration.
* **Confirm then Moderate**: send first a confirmation email to the user, then a moderator will authorize the registration.

We recommend the 'Confirm' option so that no email addresses can be added without consent.

![List_settings](img/mailman/sub-policy.png)

**Setting up parameters for incoming messages**

For mailing lists, where members can have conversations with each other, you can determine how messages addressed to the list are treated by both subscribed and unsubscribed users.

Under Settings > Message Acceptance:

For subscribed users, choose between one of these two options under 'Default action to take when a member posts to the list':

* **Hold for moderation**: a moderator has to authorize any message to be delivered to the rest of the subscribers.
* **Accept immediately**: messages from subscribers will be automatically delivered to all other subscribers, without the need for moderation. This is the standard option for mailing lists.

For unsubscribed users, choose the option you prefer under 'Default action to take when a non-member posts to the list':

* **Hold for moderation**: a moderator has to give permission for the message to be delivered to the list subscribers.
* **Reject**: all messages will be automatically rejected, notifying the sender.
* **Discard**: all messages will be automatically rejected, without notifying the sender.
* **Accept immediately**: all messages will be automatically accepted, without the need for moderation.

![Lists_policy](img/mailman/lists-policy.png)

# Activate DKIM

Go to the control panel and consults the My Applications tab > Mailman > Lists' Domains in the menu on the left column. You will see a list of all domains added from Mailman.
From this same page you can activate the DKIM key for domains you have created from the Mailman interface. Remember that the DKIM key is important to prevent sent emails from ending up in the SPAM folder.
You can find more information about DKIM on the following page: [DNS - Registro Dkim](dns/#registro-dkim).

![Activar Dkim](img/en/mailman/activate-dkim.png)

# Configure DNS for the domain or sub-domain

For the created lists to work properly, it is necessary to configure the DNS for the added domain or subdomain.

As a first step, you need to access through the control panel the required values for the DNS records.
The control panel will automatically detect the domains or subdomains activated from Mailman and will return the correct values for the DNS entries, which you will have to configure in the panel provided by your domain provider (operation external to MaadiX).

From the page My Applications > Mailman > Lists' Domains in the left column menu you access the list of all domains added from Mailman.

Click the 'View' link in the DNS column of the domain or subdomain you wish to configure.

Here you will find the necessary DNS values for a correct configuration. If you only want to use this domain or sub-domain for mailing lists, you only need to configure the DNS for the MX, TXT and DKIM values, which you will find in the 'Mail Server' table.

![List of mailman domains](img/en/domains/required-dns.png)

To create a subdomain or configure the DNS of an existing domain, you will have to enter the administration panel supplied by your provider (Gandi, Dinahosting, etc...). Unfortunately, each interface is different depending on the provider, so there is no unique way to carry out this process. See the [DNS](dns) section of this user guide for more information.
