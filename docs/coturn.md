# TURN Server

TURN servers are used to relay traffic if the direct connection between two devices (peer to peer) fails.

Thanks to [WebRTC](https://webrtc.org/) browsers are able to make direct connections between devices, sharing resources such as audio, video or screen. In this way we can make video calls simply by sharing a link and accessing it from the browser, without the need for additional software. To establish this type of communication, a protocol is needed that allows clients behind NAT to discover their public IP address and port, in order to activate the connection. The STUN protocol meets these requirements and is configured and activated by default in the Nextcloud Talk application.

In most cases, one STUN server is sufficient to enable the connection. However, in some cases, depending on the network configuration from which the devices are connected, it may be impossible for browsers to establish a direct connection with the other peers using only STUN.

To solve these problems a TURN server can be activated, capable of resolving possible incompatibilities and allowing any device to join the calls from the browser.
Activating a TURN server results in higher bandwidth consumption and increased latency. However, in some cases, it becomes the only effective solution to ensure successful connections to all devices.

The NextCloud Talk application, which allows you to create both public and private video conference rooms, can generate these error circumstances.

As a solution we have added the option of installing a TURN server from the control panel.

If you want to know more about the WebRTC + TURN connections, check this link:

[https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/](https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/)


## Installation

You will find the installation option on the 'Install Applications' page in the left menu of the control panel.
Checking the box 'Select' in 'TURN Server' will display a field, in which you will have to insert a password. This password is required to connect the Talk application to the TURN server.

![Screenshot](img/en/apps/install-coturn.png)  


Once the installation is completed, you will receive an email with the necessary data to activate the connection between the Talk application and the TURN server.
The password is not included in the email, and you will be able to see or modify it from the same control panel, in the tab My Applications -> Turn Server, of the left menu.

![Screenshot](img/en/apps/edit-coturn.png)


## Connecting Talk with TURN

Once the TURN installation process is complete, you need to install and configure the Talk extension from Nextcloud,  if you don't have it activated yet. You can do it from the Nextcloud interface by visiting the Apps -> Social & Communication section, as administrator.

![Screenshot](img/coturn/talk-install.png)

Once activated, you will have to go to Settings -> 'Talk' and insert the values you have received by email in the 'TURN Server' section.

![Screenshot](img/coturn/talk-configure.png)

In case you cannot access the mail you received, the values are as follows:

- Turn server address:
Your.server.name:5349

- Password (TURN server shared secret)
This is the password you chose when installing the application, and you can check it again or change it from the control panel, in the My Applications tab -> Turn Server, in the left menu.

- Protocol:
TCP only