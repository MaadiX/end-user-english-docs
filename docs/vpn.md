# VPN: activate accounts and client configuration

Virtual Private Network or VPN ([Virtual Private Network - Wikipedia](https://en.wikipedia.org/wiki/Virtual_private_network)) is a way to protect all your traffic through an encrypted, secure and direct connection to your server, guaranteeing the confidentiality of your navigation even in the most adverse circumstances (public or unreliable networks).

MaadiX's VPN allows you to connect to your server and manage it using a secure, encrypted connection at all times. In addition, you can also visit any Internet address by channeling your traffic through the server, allowing you to access content that might be blocked in the country where you are physically located.

To start using your MaadiX server's VPN you must perform two processes:
* Create or edit a user from your control panel to give them access to the VPN
* Install a 'client' program on the device you want to connect through the VPN

Both processes - without which you will not be able to enjoy the benefits of a VPN connection - are explained, step by step, below.

# Create or edit a user from your control panel to give them access to the VPN

1. When you are in the control panel, go to the tab '**Users**'. You can either edit an existing user or create a new one.  

2. Among the options available within the editing panel, you will see '*Activate VPN Account*'. Check the appropriate box to activate the VPN account for this particular user. If you do not have the VPN server installed, this option will not be available and you will have to install the VPN server first from the '**Install Applications**' page of the control panel.  

3. Also check the box '*Send instructions*' to send the user an e-mail with the configuration files and the corresponding instructions for the configuration of the VPN client. Remember that the instructions include all the necessary configuration data except the password, which for security reasons you must provide to the user on another channel.

![Screenshot](img/es/users/enable-vpn.png)


# Install and configure the OpenVPN client 

To establish a VPN connection with the server you need a client application, which in the case of MaadiX is OpenVPN, this being the open source solution that you can install on the server. Below, you can find a detailed tutorial for computers with Windows or Linux operating systems, as well as for Android mobiles.

## Windows

1. Download and install the OpenVPN application from [this link ](https://openvpn.net/index.php/open-source/downloads.html).

![Screenshot](img/windows-vpn/01-vpn-download.png)

2. Make a note of the location of the application you just downloaded. Once the installation process is complete, copy in the folder \config the two files inside the '/windows/` folder attached to the email that notified you of the activation of your VPN account (\*). 

a) `vpn.ovpn`    
b) `ca.crt`  

![Screenshot](img/windows-vpn/02-vpn-location.png)

Note: the default location is `C:\Program Files\OpenVPN`, so you will have to copy both files into `C:\Program Files\OpenVPN\config\`, unless during the installation you have placed the application in another folder.

3. Open the OpenVPN GUI application. A shortcut has probably been created on the desktop.

4. Enter the user and the password sent by the administrator to log in.

![Screenshot](img/windows-vpn/03-insert-user.png)

Wait a few seconds until the connection is established. To check that the connection has been made successfully, visit the website [http://cualesmiip.com/](http://cualesmiip.com/) by turning the VPN on and off. The result it gives you as 'Your real IP' should be different.

## Linux

1. Install the OpenVPN client if you don't have it installed yet (many Linux distributions include it by default).   

By console:  

    sudo apt-get install network-manager-openvpn  
    sudo apt-get install network-manager-openvpn-gnome  

    sudo restart network-manager

With the Synaptic package manager:

Applications > System Tools > Synaptic Package Manager

Search and select network-manager-openvpn and network-manager-openvpn-gnome and install them.

![Screenshot](img/linux-vpn/01-install.png)

2. Click on 'Network Settings' from the Network Manager panel (the name can also be 'Network Preferences', 'Network Connections' or other, depending on your Linux distribution).

![Screenshot](img/linux-vpn/ubuntu-edit.conn.png)

3. Look for the 'Add' or simply '+' button to add the new configuration and choose the VPN/OpenVPN option.

![Screenshot](img/linux-vpn/03-add-vpn.png)

4. Choose the option 'Import from a file'.

![Screenshot](img/linux-vpn/03-import-profile.png)

5. Select the vpn.conf file within the `/linux/` folder, which was mailed to you when your VPN account was activated (\*).

![Screenshot](img/linux-vpn/04-select-file.png)

6. Enter your user name and password to log in. Choose the password management method you prefer (the 'Always ask' option is recommended).

![Screenshot](img/linux-vpn/04-select-file.png) 

7. Return to the Network Manager to activate the newly created connection.

![Screenshot](img/linux-vpn/07-connected-vpn.png) 

Wait a few seconds until the connection is established. To check that the connection has been made successfully, visit the website [http://cualesmiip.com/](http://cualesmiip.com/) first with the VPN on and then off. The result it gives you as 'Your real IP' should be different.

## Android

1. Extract and save to your device (SD card or internal storage) the VPN configuration files you received by email (\*). 

2. Download the App in Google Play or F-Droid **OpenVpn Connect**.

[Download OpenVpn Connect from GooglePlay](https://play.google.com/store/apps/details?id=net.openvpn.openvpn&hl=es)  
[Download OpenVpn Connect from F-Droid](https://f-droid.org/app/de.blinkt.openvpn)  

3. Open the application. In the menu, choose the option 'Import'.

![Screenshot](img/android-vpn/01-import.png)

4. Select the option 'Import profile from SD card' (This option is valid even if you do not have an SD card. Look for it in your phone's internal storage.)

![Screenshot](img/android-vpn/02-profile.png)

5. Locate and select on your SD card the previously downloaded configuration file with path `/VPN-(direcciónIP)/android/android-client.ovpn`. In this folder you will also find a file called ca.crt, which must remain there for the VPN client to be configured correctly.

![Screenshot](img/android-vpn/03-choosefile.png)

6. Enter your user name and password to log in.

![Screenshot](img/android-vpn/04-user-password.png)

Wait a few seconds until the connection is established. To check that the connection has been made successfully, visit the website [http://cualesmiip.com/](http://cualesmiip.com/) first with the VPN on and then off. The result it gives you as 'Your real IP' should be different.

![Screenshot](img/android-vpn/05-connect.png)

-----

(\*): When the administrator activates a VPN account from the control panel, it can choose to send an email with instructions to install the VPN client and the required configuration files for different operating systems. If a VPN account user does not receive this mail with files and instructions, a new request must be made to the administrator. The administrator can resend it at any time from the editing panel of the particular user.


