# Create your website or application

## Choose your domain

In order to create a website accessible from any browser, you will need a domain or sub-domain.

If you already have your own domain, MaadiX allows you to activate it and configure it easily for later use on your website and applications. If you have not yet activated your domain on MaadiX, you can find instructions on how to proceed in the [following tutorial](domains.md).

Alternatively, if you do not have your own domain, you can use your *subdomain.maadix.org*, where 'subdomain' matches the name you chose when you purchased your MaadiX server. You will have to create your web or application within the folder `/var/www/html/subdominio.maadix.org`, and you will be able to visit it from the browser in your *subdominio.maadix.org*.

The following tutorial describes the steps to follow if you have your own domain. However, the process is the same for using it in your *subdomain.maadix.org* if the previous information is taken into account.

## Upload content

Once you have [activated the domain](domains.md) for your new website, you will need to upload your web or application files to the folder located in `/var/www/html/mydomain.com`.

You can access this location and easily upload files using an SFTP (Secure File Transfer Protocol) client. In case you don't have any SFTP client installed on your computer and you don't know which one to choose, [Filezilla](https://filezilla-project.org/) is one of the most widely used and simple.

Naturally, the SFTP client will ask you for a set of credentials to connect to the server:

#### With an SFTP account

If when you activate your domain you have assigned it a specific webmaster user with only SFTP access, this account will not have access through terminal. He or she will only be able to access the server using an SFTP client since they do not have SSH access. This account will be the owner of the folder `/var/www/html/mydomain.com`, so only by connecting to this account you will be able to modify the files within it.

The credentials for the connection with the Webmaster users are:

* **Server**: your *subdomino.maadix.org*.
* **Protocol**: SFTP.
* **Access mode**: normal.
* **User**: the name of the Webmaster user (Warning: it is case sensitive)
* **Password**: the password you have set for this user.

![Screenshot](img/sftp-midominio.png)

When the connection is established, as a Webmaster user you will see a folder with your name. In it, there will be a folder that gives access to `/var/www/html/mydomain.com`, or several if you are a Webmaster for other domains besides this one.

You can also have other files of your own that you have uploaded or created before. Webmaster users only have access to this area and not to all files on the system (as the Superuser does).

#### With an SSH account

If you have assigned as webmaster for your new domain an account with SSH and SFTP access, it will be able to access the server either through terminal or using an SFTP client. The way to access through SSH is similar to the one described in the previous section, but in this case the location where you land when the connection is established is different. These accounts have their own directory in `/home/USERNAME` and unlike SFTP accounts they are not caged in it. They can access other directories and perform read/write operations on files they own.    

You will have to navigate through the folders until you reach `/var/www/html/mydomain.com/`, which is where you will have to upload or create the files of your new website or application. Another faster option is to type the path `/var/www/html/mydomain.com/` in the "Remote Site" field.

Once there, you can follow the same steps described for the SFTP accounts [Webmaster](#domain-folder)

![Screenshot](img/sftp-home.png)
