# Installing applications

From the graphical interface you can install all the applications available on MaadiX. You can check the list from the page '**Install applications**'.


![Install applications](img/en/install-apps.png )


# Disabling applications

The applications can be disabled or re-enabled from the page '**My Applications** -> **See all**'.
When you disable an application the data is not deleted, so if you re-enable it at a later time you will get back the previous settings.
